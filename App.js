import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView} from 'react-native';
import { useEffect, useState } from 'react';

export default function App() {
  const [count, setCount] = useState(0);
  const [dado, setDado] = useState(0);
  function rodarDado(){
    setDado(Math.floor(Math.random() * 6) + 1);
  }
  
  return (
    <View style={styles.container}>
      <ScrollView>
      {/* <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text>
      <Text>Flamengo o Maior!!!</Text> */}
      <Text>Clique para contar:{count}</Text>
      <TouchableOpacity
        style={styles.teste}
        onPress={() => setCount(count + 1)}
      >
         <Text>Clique!</Text>
        </TouchableOpacity> 
        
        <TouchableOpacity
        style={styles.teste}
        onPress={() => rodarDado()}
        >
          <Text> rolar o dado </Text>
        </TouchableOpacity>
        <Text>Número Sorteado: {dado}</Text>
      <StatusBar style="auto" />
      </ScrollView>
    </View> 
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#add8e6',
    alignItems: 'center',
    justifyContent: 'center',
  },

  teste: {
    color: "black",
    backgroundColor: "red",
    width: 90,
    borderRadius: 25,
    alignItems: "center",

  },
});
